package appModule;

import org.openqa.selenium.WebDriver;

import PageObject.Login_Page;
import Utility.Constants;

public class SignIn {
	
	public static void Execute_Login(WebDriver driver) throws Exception{
		
		try {
			   Login_Page.Button_MyAccount(driver).click();
			   
			   Login_Page.Buton_Connect(driver).click();
				 
			   Login_Page.Field_Username(driver).sendKeys(Constants.userName1);

			   Login_Page.Field_Password(driver).sendKeys(Constants.password);
			   
			   Login_Page.Buton_Connect(driver).click();
			
		}catch(Exception e) {
			System.out.println("Erreur dans l'exécution de la méthode Execute_Login");
			throw(e);
		}	 
		   }

}
