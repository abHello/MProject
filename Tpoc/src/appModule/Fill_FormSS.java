package appModule;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import PageObject.SelfService_Form;
import Utility.Constants;

public class Fill_FormSS {
	
	
	public static void Fill(WebDriver driver) throws Exception{
		
		try {
			SelfService_Form.Company(driver).sendKeys(Constants.Company);
			
			//Select civilité
			Select sel=new Select(SelfService_Form.Civilite(driver));
			sel.selectByVisibleText("Monsieur");

			
			//Prenom, Nom
			SelfService_Form.First_Name(driver).sendKeys(Constants.First_Name);
			SelfService_Form.Last_Name(driver).sendKeys(Constants.Last_Name);
			
			//Tel, Mobile
			SelfService_Form.Tel(driver).sendKeys(Constants.Tel);
			SelfService_Form.Mobile(driver).sendKeys(Constants.Mobile);
			
			//Fonction dans l'entreprise
			SelfService_Form.Fonction(driver).sendKeys(Constants.job);
			
			//Adresse
			SelfService_Form.N_Voie(driver).sendKeys(Constants.voie);
			SelfService_Form.Batiment(driver).sendKeys(Constants.batiment);
			SelfService_Form.BP_CS(driver).sendKeys(Constants.BP);
			SelfService_Form.Postal_Code(driver).sendKeys(Constants.codePostal);
			SelfService_Form.City(driver).sendKeys(Constants.city);
			
			//Valider
			SelfService_Form.Validate(driver).click();
			
		}catch(Exception e) {
			System.out.println("Erreur dans l'exécution de la méthode Fill du formulaire self-service");
			throw(e);
		}

		
	}

}
