package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SelfService_Form {
	
	public static WebElement element;
	
	
	public static WebElement Mail(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_Email"));
		}catch(Exception e) {
			System.out.println("L'élément Mail n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Password(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_Password"));
		}catch(Exception e) {
			System.out.println("L'élément Paasword n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	

	public static WebElement Display_Password(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.className("iCheck-helper"));
		}catch(Exception e) {
			System.out.println("L'élément Display_Password n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Company(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_CompanyName"));
		}catch(Exception e) {
			System.out.println("L'élément Nom de l'entreprise n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Siret(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_SIRET"));
		}catch(Exception e) {
			System.out.println("L'élément SIRET n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement No_Siret(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.className("icheck_line-icon"));
		}catch(Exception e) {
			System.out.println("L'élément -je n'ai pas de numéro de SIRET- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Civilite(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_Title"));
		}catch(Exception e) {
			System.out.println("L'élément -Civilité- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	public static WebElement Civilite_M(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"RegistrationForm_Title\"]/option[3]"));
		}catch(Exception e) {
			System.out.println("L'élément -Civilité- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement First_Name(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_FirstName"));
		}catch(Exception e) {
			System.out.println("L'élément -Prénom- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Last_Name(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_LastName"));
		}catch(Exception e) {
			System.out.println("L'élément -Nom- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Tel(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_PhoneHome"));
		}catch(Exception e) {
			System.out.println("L'élément -Ligne directe- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Mobile(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_Mobile"));
		}catch(Exception e) {
			System.out.println("L'élément -Téléphone portable- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Service(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_Service"));
		}catch(Exception e) {
			System.out.println("L'élément -Service- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Fonction(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_Function"));
		}catch(Exception e) {
			System.out.println("L'élément -Fonction dans l'entreprise- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement N_Voie(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_Address1"));
		}catch(Exception e) {
			System.out.println("L'élément -N° et voie- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Batiment(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_Address2"));
		}catch(Exception e) {
			System.out.println("L'élément -ZI / Bâtiment- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement BP_CS(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_Address3"));
		}catch(Exception e) {
			System.out.println("L'élément -BP/CS- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Postal_Code(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_PostalCode"));
		}catch(Exception e) {
			System.out.println("L'élément -Code postal- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement City(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_City"));
		}catch(Exception e) {
			System.out.println("L'élément -Ville- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Country(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("RegistrationForm_CountryCode_chosen"));
		}catch(Exception e) {
			System.out.println("L'élément -Pays- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Newsletter(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"RegistrationForm\"]/div[5]/div[2]/div/div"));
		}catch(Exception e) {
			System.out.println("L'élément -Newsletter- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Commercial_Proposition(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"RegistrationForm\"]/div[5]/div[3]/div/div"));
		}catch(Exception e) {
			System.out.println("L'élément -Proposition commerciale- n'a pas été trouvé");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Validate(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.id("registerSubmit"));
		}catch(Exception e) {
			System.out.println("L'élément -Bouton valider- n'a pas été trouvé");
			throw(e);
		}
		return element;
	
	}
	
	
	public static WebElement MyAccount(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"home\"]/div[1]/header/section[2]/div/div/div/div[6]/div[2]/div[1]"));
		}catch(Exception e) {
			System.out.println("L'élément -email connecté- n'a pas été trouvé");
			throw(e);
		}
		return element;
	
	}
	
	
	public static WebElement EmailConnected(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.className("email"));
		}catch(Exception e) {
			System.out.println("L'élément -email connecté- n'a pas été trouvé");
			throw(e);
		}
		return element;
	
	}
	
	
	
	public static WebElement TextError(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"modal_modal\"]/div[2]/div[2]/div[2]"));
		}catch(Exception e) {
			System.out.println("L'élément -texte d'erreur- n'a pas été trouvé");
			throw(e);
		}
		return element;
	
	}
	
	
	public static WebElement MsgEmailError(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"RegistrationForm\"]/div[2]/div[2]/small[3]"));
		}catch(Exception e) {
			System.out.println("L'élément -texte d'erreur d'email- n'a pas été trouvé");
			throw(e);
		}
		return element;
	
	}
	
	
	public static WebElement InputOblig(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"RegistrationForm\"]/div[2]/div[3]/small[1]"));
		}catch(Exception e) {
			System.out.println("L'élément -texte d'erreur de champ obligatoire- n'a pas été trouvé");
			throw(e);
		}
		return element;
	
	}
	
	
	public static WebElement PasswordFormat(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"RegistrationForm\"]/div[2]/div[3]/small[3]"));
		}catch(Exception e) {
			System.out.println("L'élément -texte d'erreur de mot de passe- n'a pas été trouvé");
			throw(e);
		}
		return element;
	
	}
}
