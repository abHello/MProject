package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login_Page {
	
	public static WebElement element=null;
	
	public static WebElement Field_Username(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"ShopLoginForm_Login\"]"));
		
		
	}catch (Exception e) {
			System.out.println("L'élément userName n'a pas été trouvé dans la page de login");
		throw(e);
	}
		return element;
		}
	
	
	public static WebElement Field_Password(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"ShopLoginForm_Password\"]"));
		
		}catch(Exception e) {
			System.out.println("L'élément Password n'a pas été trouvé dans la page de login");
			throw(e);
		}
		return element;
	}
	
	public static WebElement Button_Login(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"loginForm\"]/div[4]/button"));
		}catch(Exception e) {
			System.out.println("L'élément Login n'a pas été trouvé dans la page de login");
			throw(e);
		}
		
		
		return element;
	}

	public static WebElement Button_MyAccount(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.className("quickaccess__btn"));
		}catch(Exception e) {
			System.out.println("L'élément MyAccount n'a pas été trouvé dans la page de login");
			throw(e);
		}
		
		
		return element;
	}
	
	public static WebElement Button_MyAccountDisconnected(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.className("quickaccess quickaccess__account-noconnected"));
		}catch(Exception e) {
			System.out.println("L'élément MyAccount n'a pas été trouvé dans la page de login");
			throw(e);
		}
		
		
		return element;
	}
	
	public static WebElement Logged_Email(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"home\"]/div[1]/header/section[2]/div/div/div/div[6]/div[2]/div[1]/span[2]"));
		}catch(Exception e) {
			System.out.println("L'élément Email n'a pas été trouvé dans la page de login");
			throw(e);
		}
		
		
		return element;
	}
	
	public static WebElement Buton_Connect(WebDriver driver) throws Exception{
		try {
			element=driver.findElement(By.xpath("//*[@id=\"section_main\"]/div[2]/div[2]/div[1]/span/a"));
		}catch(Exception e) {
			System.out.println("L'élément Email n'a pas été trouvé dans la page de login");
			throw(e);
		}
		
		
		return element;
	}
	
	
}

