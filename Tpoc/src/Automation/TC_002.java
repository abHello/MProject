package Automation;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObject.SelfService_Form;
import Utility.Constants;
import appModule.Fill_FormSS;

	//Vérifier qu'un message d'erreur s'affiche en cas d'utilisation d'un email déjà utilisé dans un autre compte self-service

class TC_002 {

	
	
	@Test
	void test() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", Constants.ChromeDriver);
		WebDriver driver=new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(Constants.url);
		
		
		//Mail, passeword
		
		SelfService_Form.Mail(driver).sendKeys(Constants.userName2);
		SelfService_Form.Password(driver).sendKeys(Constants.password);


		Fill_FormSS.Fill(driver);
		
		
		//Vérifier le texte affiché
		
		String TextError=SelfService_Form.TextError(driver).getText();
		Assert.assertEquals("- Cet email existe déjà. Pour vous connecter ou retrouver votre mot de passe cliquez-ici", TextError);
		
		//Fermer navigateur
		
		Thread.sleep(2000);
		driver.quit();
		
		
	}

}
