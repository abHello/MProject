package Automation;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObject.SelfService_Form;
import Utility.Constants;
import appModule.Fill_FormSS;

	//Vérifier qu'un password contenant que des caractères n'est pas accepté

class TC_005 {


	@Test
	void test() throws Exception {
		
		try{
		System.setProperty("webdriver.chrome.driver", Constants.ChromeDriver);
		WebDriver driver=new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(Constants.url);
		//driver.manage().window().maximize();
		
		
		//Mail, passeword, entreprise
		SelfService_Form.Mail(driver).sendKeys(Constants.userName2);
		SelfService_Form.Password(driver).sendKeys(Constants.passwordChar);

		Fill_FormSS.Fill(driver);
		
		//Vérifier le message d'erreur en cas de champ obligatoire non renseigné
		String ErrAlert=SelfService_Form.PasswordFormat(driver).getText();
		Assert.assertEquals("Le mot de passe doit contenir 5 caractères minimum dont 1 chiffre et 1 lettre.", ErrAlert);
		
		//Fermer navigateur
		driver.quit();
		
	}catch(Exception e) {
		throw(e);
	}
		
	}

}
