package Automation;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObject.SelfService_Form;
import Utility.Constants;
import appModule.Fill_FormSS;


	//Vérifier qu'un compte self-service est créé avec succès

class TC_001 {


	@Test
	void test() throws Exception {

		
		System.setProperty("webdriver.chrome.driver", Constants.ChromeDriver);
		
		WebDriver driver=new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(Constants.url);
		
		
		//Mail, password, 
		SelfService_Form.Mail(driver).sendKeys(Constants.userName1);
		//SelfService_Form.Mail(driver).sendKeys(Constants.Generate_Mail);
		//System.out.print("le mail généré est : "+Constants.Generate_Mail);
		SelfService_Form.Password(driver).sendKeys(Constants.password);
		Fill_FormSS.Fill(driver);
		
		//Vérifier email connecté
		SelfService_Form.MyAccount(driver).click();
		String EmailConnected=SelfService_Form.EmailConnected(driver).getText();
		Assert.assertEquals(Constants.userName1, EmailConnected);
		
		//Fermer navigateur
		driver.quit();
	

	}

}
