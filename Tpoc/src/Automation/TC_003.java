package Automation;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Utility.Constants;
import appModule.Fill_FormSS;

	//Vérifier qu'un message d'erreur est affiché en cas d'utilisation d'un format d'email incorrect

class TC_003 {


	@Test
	void test() throws Exception {
		System.setProperty("webdriver.chrome.driver", Constants.ChromeDriver);
		WebDriver driver=new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(Constants.url);
		//driver.manage().window().maximize();
		
		
		//Mail, passeword, entreprise
		PageObject.SelfService_Form.Mail(driver).sendKeys(Constants.userName3);
		PageObject.SelfService_Form.Password(driver).sendKeys(Constants.password);

		Fill_FormSS.Fill(driver);
		
		//Vérifier le message d'erreur en de format d'email non accepté
		String ErrMail=PageObject.SelfService_Form.MsgEmailError(driver).getText();
		Assert.assertEquals("Ce format est incorrect. Merci de supprimer les espaces, les caractères spéciaux et les accents", ErrMail);
		
		//Fermer navigateur
		driver.quit();
	}

}
