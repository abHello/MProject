package Automation;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObject.SelfService_Form;
import Utility.Constants;
import appModule.Fill_FormSS;

	//Vérifer que "information obligatoire" s'affiche si un champ obligatoire n'est pas rentré

class TC_004 {
	

	@Test
	void test() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", Constants.ChromeDriver);
		WebDriver driver=new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(Constants.url);
		
		
		//Mail, passeword
		SelfService_Form.Mail(driver).sendKeys(Constants.userName1);
		// Ne pas rentrer de password

		Fill_FormSS.Fill(driver);
		
		//Vérifier le message d'erreur en cas de champ obligatoire non renseigné
		String ErrAlert=PageObject.SelfService_Form.InputOblig(driver).getText();
		Assert.assertEquals("Information obligatoire", ErrAlert);
		
		//Fermer navigateur
		driver.quit();
		
		
	}

}
