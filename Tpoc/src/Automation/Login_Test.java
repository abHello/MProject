package Automation;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObject.Login_Page;
import appModule.SignIn;

class Login_Test {
	
	String userName=Utility.Constants.userName1;
	String password=Utility.Constants.password;
	String url=Utility.Constants.url1;
	String ChromeDriver=Utility.Constants.ChromeDriver;

	@Test
	void test() throws Exception {
		

		
		System.setProperty("webdriver.chrome.driver", ChromeDriver);
		WebDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    		driver.get(url);
    		
    		SignIn.Execute_Login(driver);

    	 	Login_Page.Button_MyAccount(driver).click();
    	 	System.out.println("account clicked");
    	 	String text=Login_Page.Logged_Email(driver).getText();
 
    		System.out.println(text);
    		Assert.assertEquals(userName, text);
    		
    	 	Thread.sleep(2000);
    	 	driver.quit();
    
    	 	
	}



}
